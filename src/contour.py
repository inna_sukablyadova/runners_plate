from enum import Enum
from typing import (Optional,
                    Tuple)

import cv2
import numpy as np
from scipy.spatial import distance


class ContourType(Enum):
    Price = 1
    Pricetag = 0


class Contour:
    """
    >>> Contour(bounding_rect=(1, 2, 3, 4)).area
    12
    >>> Contour(bounding_rect=(1, 2, 3, 4)).width
    3
    >>> Contour(bounding_rect=(1, 2, 3, 4)).height
    4
    >>> Contour(bounding_rect=(1, 2, 3, 4)).x_min
    1
    >>> Contour(bounding_rect=(1, 2, 3, 4)).x_max
    4
    >>> Contour(bounding_rect=(1, 2, 3, 4)).y_min
    2
    >>> Contour(bounding_rect=(1, 2, 3, 4)).y_max
    6
    >>> Contour(bounding_rect=(1, 2, 3, 4)).rectangle
    (1, 2, 4, 6)
    """

    def __init__(self,
                 contour: Optional[np.ndarray] = None,
                 bounding_rect: Optional[Tuple[int, int, int, int]] = None,
                 contour_type: ContourType = None):
        """
        :param contour: OpenCV-like contour with many points
        :param bounding_rect: tuple with four elements: x, y, width, height
        """
        if contour is None:
            if not len(bounding_rect) or any([x < 0 or isinstance(x, float)
                                              for x in bounding_rect]):
                raise ValueError
            else:
                self._bounding_rect = bounding_rect
        else:
            self._bounding_rect = None
        self._type = contour_type
        self._contour = contour
        self._area = None
        self._x_min = None
        self._y_min = None
        self._x_max = None
        self._y_max = None
        self._width = None
        self._height = None
        self._rectangle = None
        self._centroid = None
        self._relative_centroid = None

    @property
    def type(self) -> ContourType:
        return self._type

    @property
    def area(self) -> int:

        if self._area is None:
            self._area = self.width * self.height
        return self._area

    @property
    def bounding_rect(self) -> Tuple[int, int, int, int]:
        """
        :return: tuple with four elements: x, y, width, height
        """
        if self._bounding_rect is None:
            self._bounding_rect = cv2.boundingRect(self._contour)
        return self._bounding_rect

    @property
    def width(self) -> int:
        if self._width is None:
            self._width = self.bounding_rect[2]
        return self._width

    @property
    def height(self) -> int:
        if self._height is None:
            self._height = self.bounding_rect[3]
        return self._height

    @property
    def x_min(self) -> int:
        if self._x_min is None:
            self._x_min = self.bounding_rect[0]
        return self._x_min

    @property
    def y_min(self) -> int:
        if self._y_min is None:
            self._y_min = self.bounding_rect[1]
        return self._y_min

    @property
    def x_max(self) -> int:
        if self._x_max is None:
            self._x_max = self.x_min + self.width
        return self._x_max

    @property
    def y_max(self) -> int:
        if self._y_max is None:
            self._y_max = self.y_min + self.height
        return self._y_max

    @property
    def rectangle(self) -> Tuple[int, int, int, int]:
        """
        :return: tuple with four elements: x_min, y_min, x_max, y_max
        """
        if self._rectangle is None:
            self._rectangle = self.x_min, self.y_min, self.x_max, self.y_max
        return self._rectangle

    @property
    def centroid(self) -> Tuple[int, int]:
        if self._centroid is None:
            self._centroid = np.mean((self.y_min, self.y_max)), np.mean((self.x_min, self.x_max))
        return self._centroid

    def distance_to_centre(self, image_size):
        image_centroid = np.array(image_size) * np.array((0.7, 0.45))
        return distance.euclidean(self.centroid, image_centroid)

    def shift(self, x: int, y: int) -> 'Contour':
        return Contour(bounding_rect=(self.x_min + x, self.y_min + y,
                                      self.width, self.height),
                       contour_type=self.type)

    def expand(self, border: int = 20, limitations: Tuple[int, int] = None):
        if limitations:
            return Contour(bounding_rect=(max(0, self.x_min - border),
                                          max(0, self.y_min - border),
                                          min(self.width + 2 * border, limitations[1]),
                                          min(self.height + 2 * border, limitations[0])),
                           contour_type=self.type)
        else:
            return Contour(bounding_rect=(max(0, self.x_min - border),
                                          max(0, self.y_min - border),
                                          self.width + 2 * border,
                                          self.height + 2 * border),
                           contour_type=self.type)

    def contains(self, contour):
        x_within = ((contour.x_min >= self.x_min) and (contour.x_min <= self.x_max) or
                    (contour.x_max >= self.x_min) and (contour.x_max <= self.x_max))
        y_within = ((contour.y_min >= self.y_min) and (contour.y_min <= self.y_max) or
                    (contour.y_max >= self.y_min) and (contour.y_max <= self.y_max))
        return x_within and y_within


class east_contour:
    def __init__(self, cnt):
        self.points = list(cnt.items())
        # some shit
        self.x_min = int(min(filter(lambda it: it[0].startswith('x'), self.points), key=lambda t: t[1])[1])
        self.x_max = int(max(filter(lambda it: it[0].startswith('x'), self.points), key=lambda t: t[1])[1])
        self.y_min = int(min(filter(lambda it: it[0].startswith('y'), self.points), key=lambda t: t[1])[1])
        self.y_max = int(max(filter(lambda it: it[0].startswith('y'), self.points), key=lambda t: t[1])[1])
        # get height and wight
        self.height = self.y_max - self.y_min
        self.width = self.x_max - self.x_min
        # get coefs
        self.h_coef = self.opt_coef(self.height)
        self.w_coef = self.opt_coef(self.width)
        #
        self.all_x = [int(v) for k, v in self.points if k.startswith('x')]
        self.all_y = [int(v) for k, v in self.points if k.startswith('y')]
        self.rectangle = [self.y_min, self.x_min, self.y_max, self.x_max]
        # cv readable contour
        self.contour = np.array([
            [int(self.all_x[0] / 1.03), int(self.all_y[0] / 1.01)],
            [int(self.all_x[1] * 1.03), int(self.all_y[1] / 1.01)],
            [int(self.all_x[2] * 1.03), int(self.all_y[2] * 1.01)],
            [int(self.all_x[3] / 1.03), int(self.all_y[3] * 1.01)]
        ])
        self.area = cv2.contourArea(self.contour)

    def opt_coef(self, pixels):
        return int(pixels * 0.15)
