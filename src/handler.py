import json
import os

import asyncio
import cv2

from src.contour import east_contour
from src.utils import crop_image, imagebytes_to_array, warp_text, change_text_coords


class Handle:

    def __init__(self, logger, human_detector, east_detector, tes):
        self.logger = logger
        self.human_detector = human_detector
        self.east_detector = east_detector
        self.tes = tes

    def process_image(self, imagebytes):
        self.logger.info('Get image completed')
        image = imagebytes_to_array(imagebytes)
        image_shape = image.shape

        is_resize = image_shape[0] > 2000

        if is_resize:

            image = cv2.resize(image, (int(900 * image_shape[1] / image_shape[0]), 900), cv2.INTER_NEAREST)

            cv2.imwrite('tmp.png', image)

            with open('tmp.png', 'rb') as f:
                imagebytes = f.read()

            os.remove('tmp.png')

        human_contours = self.human_detector.detect(imagebytes)
        self.logger.info('Human detector completed')

        images = [crop_image(image, c) for c in human_contours]

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        self.logger.info('Loop created')
        tasks = [asyncio.gather(self.east_detector.detect(img)) for img in images]
        loop.run_until_complete(asyncio.gather(*tasks))

        east_results = [task.result()[0] for task in tasks]
        self.logger.info(f'East results {str(east_results[0])}')
        east_results = [json.loads(str(east_result)[2:-1]) for east_result in east_results]
        self.logger.info('East detector completed')

        text_contours = []
        pair_human_img_and_east_contour = []

        for img, human_contour, east_result in zip(images, human_contours, east_results):
            y_shift, x_shift = human_contour.y_min, human_contour.x_min
            for text_line in east_result['text_lines']:
                coords = text_line.keys()
                pair_human_img_and_east_contour.append([img, text_line.copy()])
                for coord in coords:
                    if 'x' in coord:
                        text_line[coord] += y_shift
                    if 'y' in coord:
                        text_line[coord] += x_shift

                text_contours.append(text_line.copy())

        cropped_text = [warp_text(human_img, east_contour(east_cnt))
                        for human_img, east_cnt in pair_human_img_and_east_contour]
        print(f'Found {len(cropped_text)} text contours')
        tes_results = [self.tes.recognition(text) for text in cropped_text]

        if is_resize:
            koef = image_shape[0] / image.shape[0]
            text_contours = [change_text_coords(text_contour.copy(), koef) for text_contour in text_contours]

        final_results = [(text_contour, tes_result, east_contour(east_cnt[1]).area)
                         for text_contour, tes_result, east_cnt in zip(text_contours,
                                                                       tes_results,
                                                                       pair_human_img_and_east_contour)
                         if self.tes.text_filter(tes_result)]

        return final_results
