import os

import numpy as np
import requests

from src.contour import Contour

HUMAN_SERVICE = os.getenv('HUMAN_SERVICE', 'http://localhost:3450/')


class HumanDetector:
    def __init__(self):
        pass

    def detect(self, image: np.ndarray):
        response = self.__send_photo(image)
        return [Contour(bounding_rect=[r[1], r[0], r[3], r[2]]) for r in response]

    def __send_photo(self, image):
        return requests.post(HUMAN_SERVICE, files={'image': image}).json()
