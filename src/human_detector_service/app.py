import os
import json

from flask import Flask, Response, request
import cv2

from src.human_detector_service.tensorflow_human_detection import DetectorAPI
from src.utils import imagebytes_to_array


model_path = os.getenv('HUMAN_MODEL_PATH',
                       'src/human_detector_service/models/'
                       'faster_rcnn_inception_v2_coco_2018_01_28/frozen_inference_graph.pb')
threshold = float(os.getenv('HUMAN__MODEL_THRESHOLD', 0.95))

odapi = DetectorAPI(path_to_ckpt=model_path)


app = Flask(__name__)


@app.route('/', methods=['POST'])
def post():
    imagebytes = request.files['image'].read()
    image = imagebytes_to_array(imagebytes)

    # shape = image.shape[1], image.shape[0]
    # img = cv2.resize(image, (1280, 720))

    boxes, scores, classes, num = odapi.processFrame(image)

    boxes = [box for box, scores, _ in zip(boxes, scores, classes) if scores > threshold]
    print(boxes)
    return Response(json.dumps(boxes), status=200)


if __name__ == '__main__':
    app.run('0.0.0.0', port=3450)
