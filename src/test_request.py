import requests
import time


img = open('data/test1.png', 'rb').read()
start = time.time()
try:
    r = requests.post('http://35.247.163.31:3456/', files={'image': img})
    print(r.status_code)
    print(r.json())

except Exception as e:
    print(e)


print(time.time() - start)
