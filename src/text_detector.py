from abc import abstractmethod
import os

import numpy as np
import aiohttp
import cv2


EAST_SERVICE = os.getenv('EAST_SERVICE', 'http://localhost:8769/')


class TextDetector:
    def __init__(self):
        pass

    @abstractmethod
    def detect(self, image: np.ndarray) -> dict:
        """
        Return detected text on image
        :param image:
        :return:
        """
        return {}


class EASTDetector(TextDetector):
    def __init__(self):
        super().__init__()

    async def detect(self, image: np.ndarray):
        async with aiohttp.ClientSession() as session:
            return await self.send_photo(session=session, image=image)

    async def send_photo(self, session, image):
        async with session.post(EAST_SERVICE, data={'image': cv2.imencode('.png', image)[1].tobytes()}) as response:
            return await response.read()
