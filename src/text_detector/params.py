PSEnet_path = 'src/text_detector/Detection/PSEnet/models/PSEnet_best.pth.tar.part'

keep_ratio = False # whether to keep ratio for image resize
manualSeed = 1234 # reproduce experiemnt
random_sample = True # whether to sample the dataset with random sampler
imgH = 32 # the height of the input image to network
imgW = 100 # the width of the input image to network
nh = 256 # size of the lstm hidden state
nc = 1
dealwith_lossnone = True # whether to replace all nan/inf in gradients to zero
# hardware
cuda = False # enables cuda
multi_gpu = False # whether to use multi gpu
ngpu = 0 # number of GPUs to use. Do remember to set multi_gpu to True!
workers = 0 # number of data loading workers

#---------------------------Parameters for PSEnet------------------------------------------------
arch='resnet50'#specify architecture
binary_th=1.0
kernel_num=7
scale=1
long_size=2240
min_kernel_area=5.0
min_area=800.0
min_score=0.93
