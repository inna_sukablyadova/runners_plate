from abc import abstractmethod
import re
import os

import json
from typing import Tuple

import tensorflow as tf
import numpy as np
import cv2
import pytesseract

import argparse


class TextRecognition:
    def __init__(self):
        pass

    @abstractmethod
    def recognition(self, image: np.ndarray) -> str:
        pass


class Tesseract(TextRecognition):
    def __init__(self):
        super().__init__()

    def recognition(self, image: np.ndarray) -> str:
        return self.postprocessing(pytesseract.image_to_string(image,
                                                               lang='enm'))

    @staticmethod
    def postprocessing(text) -> str:
        changed_text = text
        numbers = list(map(str, range(10)))
        if len(changed_text) <= 2:
            return changed_text
        for s in range(1, len(text)):
            if (text[s] == 'o' or text[s] == 'O') and (text[s - 1] in numbers and (text[min(s + 1, len(text) - 1)] in numbers or s == len(text) - 1)):
                changed_text = changed_text[:s] + '0' + changed_text[s + 1:]
        return changed_text

    @staticmethod
    def text_filter(text):
        if text in re.findall('[a-zA-Z]*[\d]+', text):
            return True
        else:
            return False


class AttentionOCR(TextRecognition):

    def __init__(self, ocr_model_path: str):
        self.__sess = tf.Session(graph=tf.Graph())
        tf.saved_model.loader.load(self.__sess, ["serve"], export_dir=ocr_model_path)
        self.__img_pl = self.__sess.graph.get_tensor_by_name("input_image_as_bytes:0")

        self.__prediction = self.__sess.graph.get_tensor_by_name("prediction:0")
        self.__probability = self.__sess.graph.get_tensor_by_name("probability:0")
        super().__init__()

    def recognize_from_bytes(self, image: bytes) -> Tuple[str, float]:
        with tf.device('/cpu:0'):
            input_feed = {self.__img_pl: image}

            output_feed = [self.__prediction, self.__probability]
            outputs = self.__sess.run(output_feed, input_feed)

            number = outputs[0]
            proba = outputs[1]
            return number.decode(), proba

    def recognition(self, image: np.ndarray) -> str:
        number, proba = '-1', 0

        success, encoded_image = cv2.imencode('.png', image)
        if success:
            number, proba = self.recognize_from_bytes(encoded_image.tobytes())
            number = self.postprocessing(number)
            if not self._check_number(number):
                number, proba = '-1', 0

        return number

    @staticmethod
    def _check_number(number):
        try:
            int(number)
            return True
        except ValueError:
            return False

    @staticmethod
    def postprocessing(text) -> str:
        changed_text = text
        numbers = list(map(str, range(10)))
        if len(changed_text) <= 2:
            return changed_text
        for s in range(1, len(text)):
            if (text[s] == 'o' or text[s] == 'O') and (
                    text[s - 1] in numbers and (text[min(s + 1, len(text) - 1)] in numbers or s == len(text) - 1)):
                changed_text = changed_text[:s] + '0' + changed_text[s + 1:]
        return changed_text

    @staticmethod
    def text_filter(text):
        if text in re.findall('[a-zA-Z]*[\d]+', text):
            return True
        else:
            return False

    def __del__(self):
        if hasattr(self, '__sess'):
            self.__sess.close()

    def __str__(self):
        params = {i.split("__")[-1]: str(self.__dict__[i]) for i in self.__dict__}
        result = {self.__class__.__name__: params}
        return json.dumps(result)


def postprocessing(text) -> str:
    changed_text = text
    numbers = list(map(str, range(10)))
    if len(changed_text) <= 2:
        return changed_text
    for s in range(1, len(text)):
        if (text[s] == 'o' or text[s] == 'O') and (text[s - 1] in numbers and (text[min(s + 1, len(text) - 1)] in numbers or s == len(text) - 1)):
            changed_text = changed_text[:s] + '0' + changed_text[s + 1:]
    changed_text = [str(s) for s in changed_text if s.isdigit()]
#     print(changed_text)
    return ''.join(changed_text)


def tess_baseline(text_save_path):
    text_boxes = [i for i in os.listdir(text_save_path) if i.endswith('.jpg')]
    log = open(f'{text_save_path}log_baseline_result.txt', 'a')
    dashed_line = '_' * 80
    head = f'{"image_path":25s}\t{"predicted_labels":25s}\tconfidence score'
    log.write(f'{dashed_line}\n{head}\n{dashed_line}\n')
    print(f'{dashed_line}\n{head}\n{dashed_line}')

    for text_box_name in text_boxes:
        text_box = cv2.imread(f'{text_save_path}{text_box_name}')
        text = pytesseract.image_to_string(text_box, lang='enm')
        text = postprocessing(text)
        if len(text) < 2:
            text = 'False'
        confidence_score = 1
        print(f'{text_save_path}{text_box_name:25s}\t{text:25s}\t{confidence_score:0.4f}')
        log.write(f'{text_save_path}{text_box_name:25s}\t{text:25s}\t{confidence_score:0.4f}\n')
    log.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--image_folder', required=True, help='path to image_folder which contains text images')
    opt = parser.parse_args()

    tess_baseline(opt.image_folder)