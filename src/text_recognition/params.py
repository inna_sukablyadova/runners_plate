""" Config """
CUDA_VISIBLE_DEVICES = 0
image_folder = 'path to image_folder which contains text images'
workers = 4
batch_size = 192
saved_model = 'model/TPS-ResNet-BiLSTM-Attn.pth'

""" Data processing """
batch_max_length = 25
imgH = 32
imgW = 100
rgb = True
character = '0123456789abcdefghijklmnopqrstuvwxyz'
sensitive = True
PAD = True

""" Model Architecture """
Transformation = 'TPS'
FeatureExtraction = 'ResNet'
SequenceModeling = 'BiLSTM'
Prediction = 'Attn'
num_fiducial = 20
input_channel = 1
output_channel = 512
hidden_size = 256