import numpy as np
import cv2

from src.contour import Contour


def crop_image(image: np.ndarray, contour: Contour):
    y, x, h, w = contour.bounding_rect
    return image[max(y, 0): min(y+h, image.shape[0]), max(x, 0): min(x+w, image.shape[1])]


def imagebytes_to_array(image_bytes):
    image = np.asarray(bytearray(image_bytes), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image


def calculate_rectangle(east_contour: dict):
    items = east_contour.items()
    x_min = int(min(filter(lambda p: 'x' in p[0], items), key=lambda p: p[1])[1])
    x_max = int(max(filter(lambda p: 'x' in p[0], items), key=lambda p: p[1])[1])
    y_min = int(min(filter(lambda p: 'y' in p[0], items), key=lambda p: p[1])[1])
    y_max = int(max(filter(lambda p: 'y' in p[0], items), key=lambda p: p[1])[1])
    res = y_min, x_min, y_max - y_min, x_max - x_min
    return res


def expand_contour(cnt: tuple):
    '''
    Expand a contour for an opt coef
    :param cnt: tuple(y, x, h, w)
    :return: expanded points
    '''
    y, x, h, w = cnt
    h_coef = opt_coef(h)
    w_coef = opt_coef(w)
    ey = int(y-h_coef)
    ex = int(x-w_coef)
    eh = int(h+h_coef*2)
    ew = int(w+w_coef*2)
    res = ey, ex, eh, ew
    return res


def opt_coef(pixels):
    # expand contour for 15 percents
    return int(pixels*0.15)


class east_contour:
    def __init__(self, cnt):
        self.score = cnt['score']
        self.points = list(cnt.items())
        self.x_min = int(min(filter(lambda it: it[0].startswith('x'), self.points), key=lambda t: t[1])[1])
        self.x_max = int(max(filter(lambda it: it[0].startswith('x'), self.points), key=lambda t: t[1])[1])
        self.y_min = int(min(filter(lambda it: it[0].startswith('y'), self.points), key=lambda t: t[1])[1])
        self.y_max = int(max(filter(lambda it: it[0].startswith('y'), self.points), key=lambda t: t[1])[1])
        # get height and wight
        self.height = self.y_max - self.y_min
        self.width = self.x_max - self.x_min
        # get coefs
        self.h_coef = self.opt_coef(self.height)
        self.w_coef = self.opt_coef(self.width)

    def opt_coef(self, pixels):
        return int(pixels * 0.15)


def warp_text(cropped_human, east_cnt):

    peri = cv2.arcLength(east_cnt.contour, True)
    approx = cv2.approxPolyDP(east_cnt.contour, 0.02 * peri, True)

    h = np.array([[0, 0], [east_cnt.width, 0], [east_cnt.width, east_cnt.height], [0, east_cnt.height]], np.float32)
    approx = np.array([item for sublist in approx for item in sublist], np.float32)
    transform = cv2.getPerspectiveTransform(approx, h)
    warp = cv2.warpPerspective(cropped_human, transform, (east_cnt.width,
                                                          east_cnt.height))

    return warp


def change_text_coords(text_contour, koef):
    coords = text_contour.keys()
    for coord in coords:
        if coord.startswith('x') or coord.startswith('y'):
            text_contour[coord] = int(koef*text_contour[coord])
    return text_contour
